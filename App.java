package Tema2.ptTema2;

import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JPanel;

public class App implements Runnable
{
	
	public int maxProcessingTime=SimFrame.timeOfProcess;
	public int minProcessingTime=1;
	public int nrOfPaydesks=SimFrame.payDesks;
	public int nrOfCustomer=SimFrame.customerNumber;
	public static int maxTime=SimFrame.finalTime;
	private Scheduler scheduler;
	
	public static SimFrame frame;
	
	private ArrayList<Customer> randomCustomers;
	public static int currentTime=0;
	
	public App()  
	{
		//frame=new SimFrame();
		
		
		scheduler=new Scheduler(nrOfPaydesks,nrOfCustomer);
		randomCustomers=new ArrayList<Customer>();
		generateCustomers();
		for(int i=0;i<nrOfCustomer;i++) 
        {
        	System.out.println(randomCustomers.get(i).getArrivalTime()+" " + randomCustomers.get(i).getProcessingTime());
        }
		
		
	}
	
	private void generateCustomers() 
	{
		
		for(int i=0;i<nrOfCustomer;i++) 
		{
			randomCustomers.add(new Customer((int)(Math.random()*(maxTime-1))+1,(int)(Math.random()*maxProcessingTime+1),(int)(Math.random()*1000)));
		}
		Collections.sort(randomCustomers);
		
		
	}
	 public static void main( String[] args )
	    {
	      
		 	frame=new SimFrame();
		 	
		 	
		 
	       
	    }
	
   

	public void run() {
		maxTime=SimFrame.finalTime;
		while(currentTime<=maxTime) 
		{
			
			System.out.println("AT TIME:"+currentTime);
			for(int i=0;i<randomCustomers.size();i++) 
			{
				
				if(randomCustomers.get(i).getArrivalTime()==currentTime)
				{
					
					int to=scheduler.dispatchCustomer(randomCustomers.get(i));
					frame.updateText("+++ Customer with id "+randomCustomers.get(i).getId() +" dispatched to "+to+" at "+currentTime+"\n");
					//System.out.print("Customer "+ randomCustomers.get(i).getArrivalTime() + " dispatched to ");
					
					randomCustomers.remove(i);
					
					i--;
				}
				
			}
			frame.redraw(scheduler);
			
			currentTime++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		frame.updateText("STATISTICS:\nAVERAGE SERVICE TIME: "+ (double)(PayDesk.service/SimFrame.payDesks)+"\n");
		frame.updateText("AVERAGE EMPTY QUEUE TIME: "+ ((double)SimFrame.finalTime-(double)(PayDesk.service/SimFrame.payDesks)));
		frame.updateText("\nMAX PEOPLE IN THE SHOP AT ONCE: "+SimFrame.maxPeak);
		frame.updateText("\nPEAK SECOND: "+SimFrame.peakHour);
	}
}
