package Tema2.ptTema2;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class PayDesk implements Runnable{

	private BlockingQueue<Customer> customers;
	private AtomicInteger waitingTime;
	public static int service=0;
	
	public PayDesk() 
	{
		setCustomers(new LinkedBlockingQueue<Customer>());
		waitingTime=new AtomicInteger();
		waitingTime.set(0);
		
	}
	public BlockingQueue<Customer> getCustomers() {
		return customers;
	}
	public void setCustomers(BlockingQueue<Customer> customers) {
		this.customers = customers;
	}
	public int getWaitingTime() 
	{
		return waitingTime.get();
	}
	public void setWaitingTime(int timeS) 
	{
		waitingTime.addAndGet(timeS);
	}
	public void addCustomer(Customer c,int min) {
		try {
			this.customers.put(c);
			waitingTime.addAndGet(c.getProcessingTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void run() {
		while(true) 
		{
			
			try {
				 if(customers.peek()!=null) {
				 
				 Customer onDuty=customers.peek();
				 int cur=App.currentTime;
				 int time=onDuty.getProcessingTime();
				 if(cur+this.getWaitingTime()<=SimFrame.finalTime) {
				 for(int i=0;i<time && cur<=SimFrame.finalTime;i++) 
				 {    
					 service++;
					 Thread.sleep(1000);
					 cur++;
					 this.waitingTime.addAndGet(-1);
				 }
				 //onDuty=customers.take();
				 customers.remove(onDuty);
				 if(cur<=App.maxTime) {
				 
				 App.frame.updateText("---Customer "+onDuty.getId()+" was processed and left the queue at "+ App.currentTime+"\n");
				 }
				 }
				 }
				 
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			
		}
		
	}
	
	
	

}
