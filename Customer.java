package Tema2.ptTema2;

public class Customer implements Comparable<Object>{
	private int arrivalTime=0;
	private int processingTime=0;
	private int id=1000;
	public Customer(int arrivalTime,int processingTime,int id) 
	{
		this.setArrivalTime(arrivalTime);
		this.setProcessingTime(processingTime);
		this.setId(id);
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}

	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		Customer a=(Customer) arg0;
		return this.arrivalTime-a.arrivalTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
}
