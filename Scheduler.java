package Tema2.ptTema2;

import java.util.ArrayList;

public class Scheduler {
	private ArrayList<PayDesk> ghiseuri;
	public int maxNrGhis;
	public int maxNrCustormer;
	
	
	public Scheduler(int maxNrGhis,int maxNrCustomer)
	{
		this.maxNrCustormer=maxNrCustomer;
		this.maxNrGhis=maxNrGhis;
		this.ghiseuri=new ArrayList<PayDesk>();
		for(int i=0;i<maxNrGhis;i++) 
		{
			PayDesk d=new PayDesk();
			ghiseuri.add(d);
			Thread t=new Thread(d);
			t.start();
			
		}
	}
	public int dispatchCustomer(Customer c) 
	{
		int minTime=ghiseuri.get(0).getWaitingTime();
		int minPoz=0;
		for(int i=1;i<maxNrGhis;i++) 
		{
			if(ghiseuri.get(i).getWaitingTime()<minTime) 
			{
				minTime=ghiseuri.get(i).getWaitingTime();
				minPoz=i;
			}
		}
		ghiseuri.get(minPoz).addCustomer(c,minPoz);
		//ghiseuri.get(minPoz).setWaitingTime(c.getProcessingTime());
		//System.out.println(minPoz+"with w time"+ghiseuri.get(minPoz).getWaitingTime());
		return minPoz;
	}

	public ArrayList<PayDesk> getGhiseuri() {
		return ghiseuri;
	}

	public void setGhiseuri(ArrayList<PayDesk> ghiseuri) {
		this.ghiseuri = ghiseuri;
	}
	
	

}
