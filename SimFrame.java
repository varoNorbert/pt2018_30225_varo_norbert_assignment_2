package Tema2.ptTema2;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

public class SimFrame extends JFrame implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int customerNumber=10;
	public static int timeOfProcess=10;
	public static int payDesks=2;
	public static int peakHour=-1;
	public static int finalTime=10;
	JPanel contentPane;
	JLabel time;
	JLabel time1;
	JLabel nrQueue;
	JLabel nrCustomers;
	JLabel maxProcessingTime;
	JLabel simTime;
	public static int maxPeak=0;
	JTextField inputSimTime;
	JTextField inputNrQueue;
	JTextField inputNrCustomers;
	JTextField inputMaxProcessingTime;
	String areaTxt="";
	ArrayList<JLabel> queues;
	int sec;
	int min;
	int start=0;
	JButton startBtn;
	JTextArea logField=new JTextArea();
	JScrollPane scroll=new JScrollPane();
	int cnt=0;
	public SimFrame() 
	{
		queues=new ArrayList<JLabel>();
		sec=App.currentTime%60;
		min=App.currentTime/60;
		setSize(600, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("Queue Simulator");
		start=0;
		
		contentPane=new JPanel();
		contentPane.setLayout(new GridBagLayout());
		
		time=new JLabel(" ");
		time1=new JLabel(" ");
		
		
		
		nrQueue=new JLabel("Set number of Paydesks:");
		nrCustomers=new JLabel("Set number of Customers:");
		maxProcessingTime=new JLabel("Set max process time:");
		simTime=new JLabel("Please set the final time of the simulation in seconds:");
		
		inputNrQueue=new JTextField();
		inputNrCustomers=new JTextField();
		inputMaxProcessingTime=new JTextField();
		inputSimTime=new JTextField();
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets = new Insets(5,5,5,5);
		c.anchor=GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.weightx = 0.5;
		simTime.setPreferredSize(new Dimension(200,30));
		simTime.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(simTime,c);
		
		c.gridx = 2;
		c.gridy = 0;
		c.gridwidth = 1;
		c.weightx = 0.5;
		inputSimTime.setPreferredSize(new Dimension(100,30));
		//nrQueue.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(inputSimTime,c);
		
		
		c.insets = new Insets(5,5,5,5);
		c.anchor=GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		c.weightx = 0.5;
		nrQueue.setPreferredSize(new Dimension(200,20));
		nrQueue.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(nrQueue,c);
		
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 1;
		nrCustomers.setPreferredSize(new Dimension(200,20));
		nrCustomers.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(nrCustomers,c);
		

		c.gridx = 2;
		c.gridy = 1;
		c.gridwidth = 1;
		maxProcessingTime.setPreferredSize(new Dimension(200,20));
		maxProcessingTime.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(maxProcessingTime,c);
		
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		c.weightx = 0.5;
		inputNrQueue.setPreferredSize(new Dimension(200,30));
		contentPane.add(inputNrQueue,c);
		
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 1;
		inputNrCustomers.setPreferredSize(new Dimension(200,30));
		contentPane.add(inputNrCustomers,c);
		

		c.gridx = 2;
		c.gridy = 2;
		c.gridwidth = 1;
		inputMaxProcessingTime.setPreferredSize(new Dimension(200,30));
		contentPane.add(inputMaxProcessingTime,c);
		
		
		startBtn=new JButton("START SIMULATION");
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 1;
		
		
		startBtn.addActionListener(this);
		startBtn.setPreferredSize(new Dimension(200,30));
		contentPane.add(startBtn,c);
		
		
		
		setContentPane(contentPane);
		pack();
		setVisible(true);
		
		
	}
	
	public void updateText(String text) 
	{
		areaTxt+=text;
		this.logField.setText(areaTxt);
		
	}
	
	public int getStart() 
	{
		return start;
	}
	public void redraw(Scheduler sc) 
	{
		
		sec=App.currentTime%60;
		min=App.currentTime/60;
		if(App.currentTime==App.maxTime)time.setText("Simulation Over");
		else time.setText("Simulation Time: "+min+":"+sec);
		time.setPreferredSize(new Dimension(200,30));
		time.setHorizontalAlignment(SwingConstants.CENTER);
		time.setFont(new Font("Arial Black", Font.PLAIN, 14));
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5,5,5,5);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 4;
		int yPoz=4;
		c.gridwidth = 1;
		c.weightx = 0.5;
		contentPane.add(time,c);
		ArrayList<PayDesk> ghiseuri=new ArrayList<PayDesk>();
		ghiseuri=sc.getGhiseuri();
		c.gridx=0;
		c.gridy=5;
		c.gridheight=4;
		c.gridwidth=2;
		scroll.setViewportView(logField);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.getVerticalScrollBar().setUnitIncrement(10);
	    scroll.setPreferredSize(new Dimension(200,200));
		contentPane.add(scroll,c);
		int peak=0;
		for(int i=0;i<ghiseuri.size();i++) 
		{	String txt="Queue "+ i+": ";
			queues.add(new JLabel());
			for(int j=0;j<ghiseuri.get(i).getCustomers().size();j++) 
			{
				txt+="O ";
				peak++;
			}
			queues.get(i).setText(txt);
			c.gridx=2;
			c.gridy=++yPoz;
			c.gridheight=1;
			c.gridwidth=1;
			queues.get(i).setPreferredSize(new Dimension(200,30));
			//queues.get(i).setHorizontalAlignment(SwingConstants.CENTER);
			queues.get(i).setFont(new Font("Arial Black", Font.PLAIN, 14));
			contentPane.add(queues.get(i),c);
		}
		if(peak>maxPeak) {maxPeak=peak;peakHour=App.currentTime;}
		
		
		this.setContentPane(contentPane);
		this.pack();
	}
	public void drawQ(int index,String txt) 
	{
		
	}
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource()==startBtn) 
		{   if(cnt==0) {
			   String input;

			   String input2;
			   String input4;
			   String input3;
			   input=inputNrCustomers.getText();
			   input2=inputNrQueue.getText();
			   input3=inputMaxProcessingTime.getText();
			   input4=inputSimTime.getText();
			   
			   if(!input.equals("") && !input2.equals("") && !input3.equals("") && !input4.equals("")) 
			   {
				   
				    customerNumber=Integer.parseInt(input);
					timeOfProcess=Integer.parseInt(input3);
					payDesks=Integer.parseInt(input2);   
					finalTime=Integer.parseInt(input4);
		       App a=new App();
		       Thread t=new Thread(a);
		       t.start();
		       cnt++;
		       startBtn.setText("Exit");
		       }
			   else 
			   {
				   JOptionPane.showMessageDialog(null,"Bad input!");
				   
			   }
			   }
		else 
		{
		    System.exit(0);
		}
		}
	}
	

}
